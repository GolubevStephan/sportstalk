package com.app.sportstalk.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.sportstalk.R
import com.app.sportstalk.data.dto.Post
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_post.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat


class PostListAdapter : RecyclerView.Adapter<PostHolder>() {

    private var items: ArrayList<Post> = ArrayList()
    var onItemClick: (item: Post) -> Unit = {}
    var isLastItemBind: (count:Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder =
        PostHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_post, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        if (position == itemCount-1) isLastItemBind(itemCount)
        if (position != 0 && position % 6 == 0)
            holder.bind(items[position], true)
        else
            holder.bind(items[position], false)

        holder.onItemClick = { this@PostListAdapter.onItemClick(it) }
    }

    fun setPostList(items: ArrayList<Post>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun addPost(message: Post) {
        items.add(message)
        notifyItemInserted(itemCount - 1)
    }
}

class PostHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    @SuppressLint("SimpleDateFormat")
    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    var onItemClick: (item: Post) -> Unit = {}

    fun bind(post: Post, isSevenPosition: Boolean) {
        view.apply {
            val params = iv_pic.layoutParams as LinearLayout.LayoutParams
            if (isSevenPosition) {
                params.width = LinearLayout.LayoutParams.MATCH_PARENT
                params.height = 700
                tv_header.visibility = View.GONE
                tv_date.visibility = View.GONE
                Picasso
                    .get()
                    .load(post.jetpack_featured_media_url)
                    .into(iv_pic)

            } else {
                tv_header.visibility = View.VISIBLE
                val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val localDateFormat = DateFormat.getDateInstance()
                params.width = 250
                params.height = 250
                Picasso
                    .get()
                    .load(post.jetpack_featured_media_url)
                    .into(iv_pic)
                tv_header.text = post.content.spanned
                tv_date.text = localDateFormat.format(df.parse(post.date))
            }

            rootView.setOnClickListener { onItemClick(post) }
        }
    }

    private fun prepareContent(content: String): String {
        return content.replace("<img.+/>".toRegex(), "")
    }
}