package com.app.sportstalk.ui.activity

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.sportstalk.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {
            if (validateForm()) signIn(et_log.text.trim().toString(), et_pass.text.trim().toString())
        }

        btn_register.setOnClickListener {
            if (validateForm()) createAccount(et_log.text.trim().toString(), et_pass.text.trim().toString())
        }

        FirebaseAuth.AuthStateListener {
            if (it.currentUser != null) {

            }
        }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = et_log.text.toString()
        if (TextUtils.isEmpty(email)) {
            et_log.error = "Required."
            valid = false
        } else {
            et_log.error = null
        }

        val password = et_pass.text.toString()
        if (TextUtils.isEmpty(password)) {
            et_pass.error = "Required."
            valid = false
        } else {
            et_pass.error = null
        }

        return valid
    }

    private fun createAccount(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (!task.isSuccessful) Toast.makeText(this, "User already exist", Toast.LENGTH_SHORT).show()
                else closeActivity()
            }
    }

    private fun signIn(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (!task.isSuccessful)
                    Toast.makeText(this, "User don`t exist. Create an account", Toast.LENGTH_SHORT).show()
                else closeActivity()
            }
    }

    private fun closeActivity() {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
        setResult(Activity.RESULT_OK)
        finish()
    }

}
