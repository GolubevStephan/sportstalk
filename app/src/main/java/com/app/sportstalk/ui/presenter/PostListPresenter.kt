package com.app.sportstalk.ui.presenter

import com.app.sportstalk.data.model.PostListModel
import com.app.sportstalk.ui.adapter.PostListAdapter
import com.app.sportstalk.ui.view.PostListView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging

class PostListPresenter {
    private val auth = FirebaseAuth.getInstance()
    private val model = PostListModel()
    private lateinit var view: PostListView
    val adapter = PostListAdapter()
    var category = "News"

    fun bind(view: PostListView) {
        this.view = view
        subscribeToTopic()
        FirebaseAuth.AuthStateListener {
            it
        }
        model.getCategoryList { list ->
            list.forEach {
                view.addTab(it.name + "")
            }
        }
        initAdapter()
    }

    fun initAdapter() {
        adapter.onItemClick = {
            view.showPostDetailScreen(it.id)
        }
        adapter.isLastItemBind = { position->
            model.getPostListByCategoryName(category, position) {list->
                list.forEach {
                    adapter.addPost(it)
                }
            }
        }
    }

    fun getPostListByCategory(categoryName: String, offset: Int) {
        /*   if (categoryName == "Shows")
               if (auth.currentUser != null)
                   model.getPostListByCategoryName(categoryName) {
                       adapter.setPostList(it)
                   }
               else view.showRegistrationScreen()
           else*/
        model.getPostListByCategoryName(categoryName, offset) {
            adapter.setPostList(it)
        }
    }

    private fun subscribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("alerts").addOnCompleteListener { task ->
            task
        }
    }

}