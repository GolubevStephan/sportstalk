package com.app.sportstalk.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportstalk.R
import com.app.sportstalk.data.RC_SIGN_IN
import com.app.sportstalk.ui.presenter.PostListPresenter
import com.app.sportstalk.ui.view.PostListView
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_posts_holder.*

class PostListActivity : AppCompatActivity(), PostListView {
    val presenter = PostListPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        setContentView(R.layout.activity_posts_holder)
        toolbar.title = ""
        presenter.bind(this)

        tl_categories.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                openPostListFragment(PostListFragment())
                presenter.category = tab.text.toString()
                presenter.getPostListByCategory(tab.text.toString(),0)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                openPostListFragment(PostListFragment())
                presenter.category = tab.text.toString()
                presenter.getPostListByCategory(tab.text.toString(),0)
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }
        })
    }

    override fun onBackPressed() {
    }

    private fun openPostListFragment(fr: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fr, fr.javaClass.simpleName)
            .commit()
    }

    private fun openPostDetailsFragment(fr: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fr, fr.javaClass.simpleName)
            .commit()
    }

    override fun showRegistrationScreen() {
        startActivityForResult(Intent(this, LoginActivity::class.java), RC_SIGN_IN)
    }

    override fun showPostDetailScreen(postID: Int) {
        val fr = PostDetailFragment()
        val args = Bundle()
        args.putInt("post_id", postID)
        fr.arguments = args
        openPostDetailsFragment(fr)
    }

    override fun addTab(text: String) {
        tl_categories.addTab(tl_categories.newTab().setText(text))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == RC_SIGN_IN) presenter.getPostListByCategory("Shows",0)
    }

}
