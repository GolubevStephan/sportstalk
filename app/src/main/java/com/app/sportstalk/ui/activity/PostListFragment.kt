package com.app.sportstalk.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.sportstalk.R
import com.app.sportstalk.ui.presenter.PostListPresenter
import kotlinx.android.synthetic.main.fragment_post_list.*

class PostListFragment: Fragment(){


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        rv_post_list.layoutManager = LinearLayoutManager(activity!!)
        rv_post_list.adapter = (activity!! as PostListActivity).presenter.adapter
    }

}