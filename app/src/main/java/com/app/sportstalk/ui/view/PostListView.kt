package com.app.sportstalk.ui.view

interface PostListView{
    fun addTab(text: String)
    fun showPostDetailScreen(postID: Int)
    fun showRegistrationScreen()
}