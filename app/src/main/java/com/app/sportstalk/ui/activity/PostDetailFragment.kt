package com.app.sportstalk.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.sportstalk.R
import com.app.sportstalk.data.Api
import com.app.sportstalk.data.dto.Post
import com.app.sportstalk.data.pinterest
import com.app.sportstalk.data.twitter
import com.app.sportstalk.data.whatsUP
import com.app.sportstalk.ui.adapter.ImageListAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_post_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import java.util.regex.Pattern

class PostDetailFragment : Fragment() {

    private lateinit var post: Post
    private val paragraphsTextList: ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_post_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadPostDetails()
    }

    private fun loadPostDetails() {
        GlobalScope.launch(Dispatchers.Main) {
            Api.get().getPostDetails(arguments!!.getInt("post_id", 0)).await().body().let {
                if (it == null) {
                    toast("Cannot load post details")
                    return@launch
                } else {
                    post = it
                    setupUi()
                }
            }
        }
    }

    private fun setupUi() {
        val formattedText = post.content.spanned.toString().replace("\n", "\n\n")
        val adapter = ImageListAdapter()

        rv_image_list.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_image_list.adapter = adapter

        tv_title.text = post.title.spanned
        Picasso.get().load(post.jetpack_featured_media_url).into(iv_cover)

        ic_facebook.setOnClickListener { sendIntentToFb(post.link.toString()) }
        ic_twitter.setOnClickListener { sendIntentToTwitter(post.link.toString()) }
        ic_pinterest.setOnClickListener { sendIntentToPinterst(post.link.toString()) }
        ic_whats_up.setOnClickListener { sendIntentToWhatsUp(post.link.toString()) }

        val linkList: ArrayList<String> = extractUrls(post.content.rendered)
        val imgList: ArrayList<String> = ArrayList()
        val sortedImgList: ArrayList<String> = ArrayList()
        var linksWithoutImg = ""

        linkList.forEach {
            if (!it.contains(".jpg")) linksWithoutImg = "$linksWithoutImg $it"
            if (it.contains(".jpg")) imgList.add(it)
        }

        imgList.forEachIndexed { index, str ->
            if (index == 0) sortedImgList.add(str)
            else {
                if (getImgName(imgList[index]).substring(0, 4) != getImgName(imgList[index - 1]).substring(0, 4))
                    sortedImgList.add(str)
            }
        }

        if (linksWithoutImg.contains(".html")) web_view.visibility = View.GONE
        if (linksWithoutImg.isEmpty()) web_view.visibility = View.GONE

        extractText(post.content.rendered)
        setupWebView(linksWithoutImg)

        var text1 = ""
        var text2 = ""
        var text3 = ""

        if (paragraphsTextList.isEmpty()) {
            tv_content_1.visibility = View.GONE
            iv_cover_1.visibility = View.GONE
            tv_content_2.visibility = View.GONE
            iv_cover_2.visibility = View.GONE
            tv_content_3.visibility = View.GONE
            rv_image_list.visibility = View.GONE
        } else
            when (paragraphsTextList.size) {
                1 -> {
                    paragraphsTextList.forEachIndexed { index, str -> if (index in 0..0) text1 = "$text1 \n $str" }
                    tv_content_1.visibility = View.VISIBLE
                    iv_cover_1.visibility = View.VISIBLE
                    tv_content_2.visibility = View.GONE
                    iv_cover_2.visibility = View.GONE
                    tv_content_3.visibility = View.GONE
                    rv_image_list.visibility = View.GONE
                }
                2 -> {
                    text1 = paragraphsTextList[0]
                    paragraphsTextList.forEachIndexed { index, str -> if (index in 1..1) text2 = "$text2 \n $str" }
                    tv_content_1.visibility = View.VISIBLE
                    iv_cover_1.visibility = View.VISIBLE
                    tv_content_2.visibility = View.VISIBLE
                    iv_cover_2.visibility = View.VISIBLE
                    tv_content_3.visibility = View.GONE
                    rv_image_list.visibility = View.GONE
                }
                3 -> {
                    text1 = paragraphsTextList[0]
                    text2 = paragraphsTextList[1]
                    paragraphsTextList.forEachIndexed { index, str -> if (index >= 2) text3 = "$text3 \n $str" }
                    tv_content_1.visibility = View.VISIBLE
                    iv_cover_1.visibility = View.VISIBLE
                    tv_content_2.visibility = View.VISIBLE
                    iv_cover_2.visibility = View.VISIBLE
                    tv_content_3.visibility = View.VISIBLE
                    rv_image_list.visibility = View.VISIBLE
                }
               else -> {
                    text1 = "${paragraphsTextList[0]}\n${paragraphsTextList[1]}\n${paragraphsTextList[2]}"
                    text2 = paragraphsTextList[3]
                    paragraphsTextList.forEachIndexed { index, str -> if (index >= 3) text3 = "$text3 \n $str" }
                    tv_content_1.visibility = View.VISIBLE
                    iv_cover_1.visibility = View.VISIBLE
                    tv_content_2.visibility = View.VISIBLE
                    iv_cover_2.visibility = View.VISIBLE
                    tv_content_3.visibility = View.VISIBLE
                    rv_image_list.visibility = View.VISIBLE
                }

            }

        tv_content_1.text = text1
        tv_content_2.text = text2
        tv_content_3.text = text3

        if (sortedImgList.isEmpty()) {
            iv_cover_1.visibility = View.GONE
            iv_cover_2.visibility = View.GONE
            rv_image_list.visibility = View.GONE
        } else
            when (sortedImgList.size) {
                1 -> {
                    Picasso.get().load(sortedImgList[0]).into(iv_cover_1)
                    iv_cover_2.visibility = View.GONE
                    rv_image_list.visibility = View.GONE
                }
                2 -> {
                    Picasso.get().load(sortedImgList[0]).into(iv_cover_1)
                    Picasso.get().load(sortedImgList[1]).into(iv_cover_2)
                    rv_image_list.visibility = View.GONE
                }
                else -> {
                    Picasso.get().load(sortedImgList[0]).into(iv_cover_1)
                    Picasso.get().load(sortedImgList[1]).into(iv_cover_2)
                    sortedImgList.removeAt(0)
                    sortedImgList.removeAt(1)
                    adapter.setPostList(sortedImgList)
                }
            }

        /* val imgGetter = Html.ImageGetter {
             val drawable = resources.getDrawable(R.drawable.ic_whats_up)
             drawable!!.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
             it
             drawable
         }
         tv_content_2.text = Html.fromHtml(post.content.rendered, imgGetter, null);*/

    }

    fun getImgName(link: String) = link.substring(link.lastIndexOf('/') + 1)

    fun extractUrls(text: String): ArrayList<String> {
        val containedUrls = ArrayList<String>()
        val urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)"
        val pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE)
        val urlMatcher = pattern.matcher(text)

        while (urlMatcher.find()) {
            containedUrls.add(
                text.substring(
                    urlMatcher.start(0),
                    urlMatcher.end(0)
                )
            )
        }
        return containedUrls
    }

    fun extractText(html: String) {
        val doc = Jsoup.parse(html)
        val paragraphs = doc.select("p")
        paragraphs.forEach { paragraphsTextList.add(it.text()) }
    }

    fun setupWebView(allLinks: String) {
        val html =
            ("<!DOCTYPE html><html> <head> <meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"target-densitydpi=high-dpi\" /> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <link rel=\"stylesheet\" media=\"screen and (-webkit-device-pixel-ratio:1.5)\" href=\"hdpi.css\" /></head> <body style=\"background:black;margin:0 0 0 0; padding:0 0 0 0;\"> <iframe id=\"sc-widget " +
                    "\" width=\"100%\" height=\"300\"" + // Set Appropriate Width and Height that you want for SoundCloud Player

                    " src=\"" + allLinks   // Set Embedded url

                    + "\" frameborder=\"no\" scrolling=\"no\"></iframe>" +
                    "<script src=\"https://w.soundcloud.com/player/api.js\" type=\"text/javascript\"></script> </body> </html> ")
        web_view.settings.javaScriptEnabled = true
        web_view.settings.loadWithOverviewMode = true
        web_view.settings.useWideViewPort = true
        web_view.loadDataWithBaseURL("", html, "text/html", "UTF-8", "")
    }

    private fun sendIntentToFb(urlToShare: String) {
        var intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare)

        var facebookAppFound = false
        val matches = activity!!.packageManager.queryIntentActivities(intent, 0)
        for (info in matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName)
                facebookAppFound = true
                break
            }
        }
        if (!facebookAppFound) {
            val sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=$urlToShare"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
        }
        startActivity(intent)
    }

    private fun sendIntentToPinterst(urlToShare: String) {
        var intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare)

        var facebookAppFound = false
        val matches = activity!!.packageManager.queryIntentActivities(intent, 0)
        for (info in matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(pinterest)) {
                intent.setPackage(info.activityInfo.packageName)
                facebookAppFound = true
                break
            }
        }
        if (!facebookAppFound) {
            val sharerUrl = "http://pinterest.com/pin/create/button/?url=$urlToShare"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
        }
        startActivity(intent)
    }

    private fun sendIntentToWhatsUp(urlToShare: String) {
        var intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare)

        var facebookAppFound = false
        val matches = activity!!.packageManager.queryIntentActivities(intent, 0)
        for (info in matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(whatsUP)) {
                intent.setPackage(info.activityInfo.packageName)
                facebookAppFound = true
                break
            }
        }
        if (!facebookAppFound) {
            val sharerUrl = "https://wa.me/?text=$urlToShare"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
        }
        startActivity(intent)
    }

    private fun sendIntentToTwitter(urlToShare: String) {
        var intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare)

        var facebookAppFound = false
        val matches = activity!!.packageManager.queryIntentActivities(intent, 0)
        for (info in matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(twitter)) {
                intent.setPackage(info.activityInfo.packageName)
                facebookAppFound = true
                break
            }
        }
        if (!facebookAppFound) {
            val sharerUrl = "https://twitter.com/intent/tweet?text=WRITE YOUR MESSAGE HERE &url=$urlToShare"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
        }
        startActivity(intent)
    }

    private fun toast(text: String) {
        Toast.makeText(activity!!, text, Toast.LENGTH_SHORT).show()
    }
}