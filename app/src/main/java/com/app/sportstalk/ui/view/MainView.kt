package com.app.sportstalk.ui.view

interface MainView {
    fun showToast(text: String)
}