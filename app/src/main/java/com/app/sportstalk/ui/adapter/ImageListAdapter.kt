package com.app.sportstalk.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.sportstalk.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_image.view.*

class ImageListAdapter : RecyclerView.Adapter<ImageHolder>() {

    private var items: ArrayList<String> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder =
        ImageHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_image, parent, false))
    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        Picasso.get().load(items[position]).into(holder.view.iv_img)
    }

    fun setPostList(items: ArrayList<String>) {
        this.items = items
        notifyDataSetChanged()
    }
}

class ImageHolder(val view: View) : RecyclerView.ViewHolder(view)