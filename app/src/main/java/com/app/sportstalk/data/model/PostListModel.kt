package com.app.sportstalk.data.model

import com.app.sportstalk.data.Api
import com.app.sportstalk.data.dto.Category
import com.app.sportstalk.data.dto.Post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PostListModel {
    private val api = Api.get()
    private val categoryNameList =
        arrayListOf("Latests News", "Football", "Camogie", "Hurling", "Shows")
    private val selectedCategory: ArrayList<Category> = ArrayList()


    fun getAllPostList(isListFetched: (list: ArrayList<Post>) -> Unit) {
        GlobalScope.launch {
            val response = api.gettAllPostList().await()
            if (response.isSuccessful) CoroutineScope(Dispatchers.Main).launch { isListFetched(response.body()!!) }
        }
    }

    fun getPostListByCategoryName(categoryName: String, bindedPosts: Int, isListFetched: (list: ArrayList<Post>) -> Unit) {
        GlobalScope.launch {
            var categoryID = -1
            selectedCategory.forEach {
                if (categoryName == it.name) categoryID = it.id
            }

            val response = api.getPostListByCategory(categoryID, bindedPosts).await()
            if (response.isSuccessful) CoroutineScope(Dispatchers.Main).launch { isListFetched(response.body()!!) }
        }
    }

    fun getCategoryList(isListFetched: (list: ArrayList<Category>) -> Unit) {
        GlobalScope.launch {
            val response = api.getCategoryList().await()
            val responseHurling = api.getHurlingCategory().await()
            val responseLatest = api.getLatestNewsCategory().await()
            val responseShows = api.getShowCategory().await()
            if (response.isSuccessful
                && responseHurling.isSuccessful
                && responseLatest.isSuccessful
                && responseShows.isSuccessful
            )
                CoroutineScope(Dispatchers.Main).launch {
                    val newsCategory = Category(0, "News")
                    var footballCategory = Category(0, "Football")

                    response.body()!!.forEach { if (it.name == footballCategory.name) footballCategory = it }
                    newsCategory.id = responseLatest.body()!!.id

                    selectedCategory.add(newsCategory)
                    selectedCategory.add(footballCategory)
                    selectedCategory.add(responseHurling.body()!!)
                    selectedCategory.add(responseShows.body()!!)
                    isListFetched(selectedCategory)
                }
        }
    }
}