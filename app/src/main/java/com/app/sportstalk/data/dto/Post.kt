package com.app.sportstalk.data.dto

import android.text.Spanned
import androidx.core.text.HtmlCompat
import java.net.URL

data class Post(
    var id: Int,
    var date: String,
    var date_gmt: String,
    var link: URL,
    var title: Title,
    var content: Content,
    var excerpt: Excerpt,
    val jetpack_featured_media_url: String
)

data class Title(
    private var rendered: String
) {
    val spanned: Spanned
        get() = HtmlCompat.fromHtml(rendered, HtmlCompat.FROM_HTML_MODE_COMPACT)
}

data class Content(
    var rendered: String
) {
    val spanned: Spanned
        get() = HtmlCompat.fromHtml(
            rendered.replace("<img.+/>".toRegex(), ""),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )
}

data class Excerpt(
    var rendered: String
)
