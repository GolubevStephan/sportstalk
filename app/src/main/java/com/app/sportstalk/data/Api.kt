package com.app.sportstalk.data

import com.app.sportstalk.data.dto.Category
import com.app.sportstalk.data.dto.Post
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import timber.log.Timber

interface Api{

    @GET("wp-json/wp/v2/posts/{id}")
    fun getPostDetails(@Path("id") id: Int): Deferred<Response<Post>>

    @GET("wp-json/wp/v2/posts/")
    fun gettAllPostList(): Deferred<Response<ArrayList<Post>>>

    @GET("/wp-json/wp/v2/categories")
    fun getCategoryList(): Deferred<Response<ArrayList<Category>>>

    @GET("/wp-json/wp/v2/categories/639")
    fun getShowCategory(): Deferred<Response<Category>>

    @GET("/wp-json/wp/v2/categories/79")
    fun getHurlingCategory(): Deferred<Response<Category>>

    @GET("/wp-json/wp/v2/categories/325")
    fun getLatestNewsCategory(): Deferred<Response<Category>>

    @GET("wp-json/wp/v2/posts/")
    fun getPostListByCategory(@Query("categories") id: Int,
                              @Query("offset") offset: Int): Deferred<Response<ArrayList<Post>>>

    companion object {
        fun get(): Api {
            return retrofit.create(Api::class.java)
        }

        private val retrofit: Retrofit = Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger
                    { message -> Timber.i(message) }).apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                    .build()
            )
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://sportstalk.ie/")
            .build()
        }
}