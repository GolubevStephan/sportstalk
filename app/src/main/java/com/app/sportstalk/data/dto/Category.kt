package com.app.sportstalk.data.dto

data class Category(
    var id: Int,
    var name: String
)